# -*- coding: utf-8 -*-
"""
Copyright (C) 2017 Charles Jackson - All Rights Reserved
Created on Sun Sep 16 18:57:00 2017

@author: Charles Jackson
"""
import dataio
import wordchain
import chainparser2
import csv
import re


def basicstem(string):
    l = len(string)
    if string[l - 1] == 's':
        string = string[:l-1]
    elif string[l - 2:l] == 'ed':
        string = string[:l - 2]
    elif l > 5 and string[l - 3:l] == 'ing':
        string = string[:l - 3]
    return string

class Bot:
    _UNKNOWN = 'unknown'
    def __init__(self,datapath):
        self.table = dataio.Table(datapath)
        self.patternlist = [['subject','+','linkingverb','>>','negative_concern','+'], \
                            ['subject', '+', 'linkingverb', '>>', 'positive_concern', '<', 'negator'], \
                            ['negative_concern', '+', '!<', 'linkingverb', '>', 'preposition', '>', 'subject', '+'], \
                            ['subject', '+', 'negative_concern', '+'], \
                            ['negative_concern', '+', 'subject', '+'], \
                            ['subject', '+', '>', 'negator', 'positive_concern'], \
                            ['negative_concern','+', '!<','linkingverb', 'preposition', 'positive_concern'],\
                            #['negative_concern','+', '!<', 'linkingverb', 'preposition', '>', 'orientation'],\
                            #['subject','+', 'preposition', 'orientation'],\
                            ['negator','positive_concern']]



    def get_concern_info(self, chain):

        parser = chainparser2.ChainParser()

        string = Bot._UNKNOWN
        subchains = None
        for pattern in self.patternlist:

            parser.setpattern(pattern)
            parser.search(chain)
            match = parser.getmatches()


        if len(match.worditems)>0:
            string = '; '.join(match.worditems)
            subchains = match.chainitems

        return (string, subchains)


    def get_keywords(self, chain, subchains, delimiter = '; '):
        components = set()
        maincomponents = {}
        concerns = set()
        mainconcerns = {}
        orientation = set()
        categories = set()
        categorizer = wordchain.Categorizer().concern_categories

        for link in chain:

            if 'negative_concern' in link.categories or 'positive_concern' in link.categories:
                concerns.add(basicstem(link.word))
                iteratedictionary(mainconcerns,basicstem(link.word))
            elif 'subject' in link.categories:
                components.add(link.word)
                iteratedictionary(maincomponents, link.word)

            for key in categorizer.keys():
                if link.word in categorizer[key]:
                    categories.add(key)



        if subchains is not None:
            for chain in subchains:
                for link in chain:
                    if 'orientation' in link.categories:
                        orientation.add(link.word)

        mainsubject = self.__best_guess_loop(maincomponents) #main and secondary subject tuple
        mainconcern = self.__best_guess_loop(mainconcerns) #main and secondary concern tuple
        subject1 = mainsubject[0]
        subject2 = mainsubject[1]
        concern1 = mainconcern[0]
        concern2 = mainconcern[1]

        return (delimiter.join(components), delimiter.join(concerns), delimiter.join(orientation),\
                subject1, subject2, concern1, concern2, delimiter.join(categories))




    def __best_guess_loop(self,dictionary):
        output1 = Bot._UNKNOWN
        output2 = Bot._UNKNOWN
        minval = 1 #minimum value to alter the output
        minval2nd = 0 #find the secondary concern
        for item in dictionary.items():
            if item[1] > minval:
                minval = item[1]
                output1 = item[0]
            elif item[1] <= minval and item[1] >minval2nd:
                minval2nd = item[1]
                output2 = item[0]

        return (output1, output2)

    def prepend_data(self, column = []):
        chainbuilder = wordchain.ChainBuilder()


        for row in self.table.data:
            strings = []
            for val in column:
                strings.append(row[val-1])
            string = ' and '.join(strings) #concatenate all columns

            chain = chainbuilder.getwordchain(string)
            strchain = self.get_concern_info(chain)
            keywords = self.get_keywords(chain, strchain[1])

            self.__row_insert(row, keywords, strchain)


        #print(self.table.data)


    def single_str_anaylsis(self, string):
        chainbuilder = wordchain.ChainBuilder()
        row = []
        chain = chainbuilder.getwordchain(string)
        strchain = self.get_concern_info(chain)
        keywords = self.get_keywords(chain, strchain[1])

        self.__row_insert(row,keywords,strchain)

        return row

    def export(self, path):
        headers = ['Concern List','First Rate Subject','First Rate Concern',\
                   'Component keywords', 'Concern keywords', 'Concern Categories','Orientation keywords'] + self.table.headers
        with open(path, 'wb') as csvfile:
            writer = csv.writer(csvfile)

            writer.writerow(headers)
            for row in self.table.data:
                writer.writerow(row)

    def __row_insert(self, row, keywords, strchain):
        row.insert(0, keywords[2])  # insert orientation
        row.insert(0, keywords[7])  # insert concern categories
        row.insert(0, keywords[1])  # insert concern keywords
        row.insert(0, keywords[0])  # insert components keywords
        #row.insert(0, keywords[6])  # insert secondary concern
        row.insert(0, keywords[5])  # insert main concern
        #row.insert(0, keywords[4])  # insert secondary subject
        row.insert(0, keywords[3])  # insert main subject
        row.insert(0, strchain[0])  # insert matches

class DTCBot:
    def __init__(self,datapath):
        self.table = dataio.Table(datapath)

def standardoutput(inputfilepath, outputfolderpath, columnlist):
    bot = Bot(inputfilepath)
    print('')
    print('Loading data...')
    try:
        print('Processing data...')
        bot.prepend_data(columnlist)
        bot.export(outputfolderpath + r'\output.csv')
        print('Done! If no error messages occured above')
        print('output.csv should be at your selected output folder')
    except IndexError:
        print('\nThere was some trouble reading the input columns')
        print('Make sure a .csv file was used and the given columns are in range')
    except IOError:
        print('\nThere was some trouble with the input or output file')
        print('Please ensure that the input file is a csv file')
        print('Also make sure the output file is not already open (I cannot read an open file)')



def iteratedictionary (dictionary, key, val = 1):

    if dictionary.has_key(key):
        dictionary[key] += val
    else:
        dictionary[key] = val



