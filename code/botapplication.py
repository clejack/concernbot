# -*- coding: utf-8 -*-
"""
Copyright (C) 2017 Charles Jackson - All Rights Reserved
Created on Sun Sep 16 18:57:00 2017

@author: Charles Jackson
"""
import Tkinter as tk, tkFileDialog
import pickle
import re
import os
import bots

class Application(tk.Frame):
    def __init__(self):
        self.persistence = 'pathvariables.txt'
        self.root = tk.Tk()
        self.root.title('Customer Concern Bot')
        tk.Frame.__init__(self, self.root)

        self.textlist = []

        self.grid()

        self.setup = SetupSection(self)
        self.command = CommandSection(self)

        print(self.textlist)


        def sendmessage(self, note):
            m = tk.Message(tk.Tk(), text=note)
            m.pack()

class CommandSection:

    def __init__(self,parent):

        self.button_width = 25
        self.entry_width = 50
        self.label_width = 50
        self.parent = parent

        # grid for starting analysis
        self.commandgrid = tk.Frame(master=parent.root)
        self.commandgrid.grid(row=2, column=1)
        self.commandgrid['pady'] = 5

        self.startbutton = makegridbutton(self.commandgrid, "Start", self.button_width, 1, 1)
        self.startbutton['command'] = lambda: self._start()

        self.savebutton = makegridbutton(self.commandgrid, "Save", self.button_width, 1, 2)
        self.savebutton['command'] = lambda: self._savetext()

        self.loadbutton = makegridbutton(self.commandgrid, "Load", self.button_width, 1, 3)
        self.loadbutton['command'] = lambda: self._loadtext()

    def _savetext(self):
        myfile = open(self.parent.persistence, 'wb')
        for item in self.parent.textlist:
            pickle.dump(item.get(), myfile)

        myfile.close()

    def _loadtext(self):
        myfile = open(self.parent.persistence, 'rb')
        for item in self.parent.textlist:
            item.set(pickle.load(myfile))
        myfile.close()

    def _start(self):
        i = self.parent.setup.inputtext.get()
        o = self.parent.setup.outputtext.get()
        c = getcollist(self.parent.setup.columntext)
        bots.standardoutput(i,o,c)

class SetupSection:

    def __init__(self, parent):

        self.parent = parent
        self.button_width = 15
        self.entry_width = 50
        self.label_width = 50

        # grid for setting paths and info
        self.setupgrid = tk.Frame(master=parent.root)
        self.setupgrid.grid(row=1, column=1)

        self.inputtext = tk.StringVar()
        self.outputtext = tk.StringVar()
        self.columntext = tk.StringVar()
        parent.textlist += [self.inputtext,self.outputtext,self.columntext]


        self.inputlabel = makelabel(self.setupgrid, "Input File (.csv)", self.label_width, 1, 1)
        self.inputentry = make_entry_field(self.setupgrid, self.inputtext, self.entry_width, 2, 1)
        self.inputbutton = makegridbutton(self.setupgrid,'Select File',self.button_width,2,2)
        self.inputbutton['command'] = lambda: setPath(self.inputtext)

        self.outputlabel = makelabel(self.setupgrid, "Output Folder: will receive output.csv", self.label_width, 3, 1)
        self.outputentry = make_entry_field(self.setupgrid, self.outputtext, self.entry_width, 4, 1)
        self.outputbutton = makegridbutton(self.setupgrid, 'Select Folder', self.button_width, 4, 2)
        self.outputbutton['command'] = lambda: setPath(self.outputtext,False)

        self.columnlabel = makelabel(self.setupgrid, "Narrative Columns: accepts integer values", self.label_width, 5,1)
        self.columnentry = make_entry_field(self.setupgrid, self.columntext, self.entry_width, 6, 1)


def setPath(entrytext, getfile = True):
    if getfile:
        path = tkFileDialog.askopenfilename()
        path = os.path.abspath(path)
        entrytext.set(path)
    else:
        path = tkFileDialog.askdirectory()
        path = os.path.abspath(path)
        entrytext.set(path)


def getcollist(stringvar):
    regex = re.compile(r'\d+')
    matches = regex.findall(stringvar.get())
    collist = []
    if matches:
        for match in matches:
            collist.append(int(match))
    return collist


def makegridbutton(parent, buttontext, buttonwidth, gridrow,gridcol ):
    mybutton = tk.Button(parent)
    mybutton['text'] = buttontext
    mybutton['width'] = buttonwidth
    mybutton.grid(row = gridrow, column = gridcol)

    return mybutton

def make_entry_field(parent, stringvar, width, gridrow,gridcol):
    myentry = tk.Entry(parent,textvariable = stringvar)
    myentry['width'] = width
    myentry.grid(row = gridrow, column = gridcol)
    return myentry

def makelabel(parent, text, width, gridrow, gridcol):
    label = tk.Label(parent)
    label['width'] = width
    label['text'] = text
    label.grid(row = gridrow, column = gridcol)
    return  label



def on_closing(root):
    try:
        root.destroy()
    except Exception:
        pass


def main():
    app = Application()
    app.root.protocol('WM_DELETE_WINDOW', lambda: on_closing(app.root))
    app.mainloop()


if __name__ == '__main__':
    main()