# -*- coding: utf-8 -*-
"""
Copyright (C) 2017 Charles Jackson - All Rights Reserved
Created on Sun Aug 27 18:37:46 2017

@author: Charles Jackson
"""

# subject - in automotive terms this is general a component with a PN; i.e. shock
# concern - relevant term to defining a complaint that is not a component; i.e. leak
# negator - any word that negates an aspect; i.e. no, not, isn't, etc.
# orientation - left, front, lf, etc.
# noun - person, place, thing
# linking verb - shows action and points the subject/noun
# verb - shows action
# adjective - describes a noun
# adverb - describes a noun, a verb, or an adjective--generally ends in ly
# gerund - a verb appended with 'ing' that functions as a noun
# preposition - tends to show location/directionality in more general terms; i.e. on, above, etc
# determiner - tends to point to a noun; i.e. a, an, the, one, some

import os
import dataio
import re

class WordChain:
    def __init__(self):
        self.head = None
        self.tail = None
        self.active_node = None  # store active node when analyzing the chain
        self.reversed = False
        self.length = 0



    def add(self, word):
        n = WordNode(word,None)
        self.length = self.length + 1
        if self.head is None:
            n = WordNode(word,None)
            self.head = n
            self.tail = n
        else:
            n = WordNode(word,self.tail)
            self.tail.nextword = n
            self.tail = n
        return n

    def addnode(self,wordnode):
        n = WordNode(wordnode.word,None)
        n.first_category = wordnode.first_category
        n.categories = wordnode.categories
        n.exclusive = wordnode.exclusive

        self.length = self.length+1
        if self.head is None:
            n.prevword = None
            self.head = n
            self.tail = n
        else:
            n.prevword = self.tail
            self.tail.nextword = n
            self.tail = n
        return n


    def reset_length(self):
        self.length = 0
        for link in self:
            self.length = self.length + 1

    def str_tuple(self):
        words = ''
        categories = ''

        for item in self:
            words = words + item.word + ' '
            categories = categories + item.first_category + ' '
        return (words,categories)

    def __str__(self):
        words = ''
        for item in self:
            words = words + item.word + ' '
        return words

    def __len__(self):
        return self.length

    def __iter__(self):
        self.active_node = self.head
        return self

    def __getitem__(self, index):
        output = self.head
        for i in range(0, index):
            output = self.next()
        return output

    def next(self):
        if self.active_node is None:
            raise StopIteration
        else:
            temp = self.active_node
            self.active_node = self.active_node.nextword
            return temp

class WordNode:
    def __init__(self, word, parent):
        self.nextword = None
        self.prevword = parent

        self.word = word
        self.first_category = '' # the first category found for this word

        # since words can have multiple categories, this string-boolean dictionary marks found categories as True
        self.categories =  set()

        #exclusive determines whether only one category was found
        #This is of interest particularly when a subject (component) or concern is exclusive
        #most likely this will be a mispelling or special word not found in the general dictionaries
        self.exclusive = False

    def _nullify(self):
        self.word = '|'
        self.first_category = '|'
        self.categories = set()

    def __str__(self):
        return self.word + ' ' + self.first_category

class Categorizer:
    UNKNOWN = 'unknown'

#   Reminder: prepending everything with the letter 'p' was meant to denote a path variable

    ROOT = os.path.dirname(os.getcwd())
    padjectives = ROOT + r'\knowledge\adjectives.csv'
    padverbs = ROOT + r'\knowledge\adverbs.csv'
    pnegative_concerns = ROOT + r'\knowledge\negative_concerns.csv'
    ppositive_concerns = ROOT + r'\knowledge\positive_concerns.csv'
    pdeterminers = ROOT + r'\knowledge\determiners.csv'
    pgerunds = ROOT + r'\knowledge\gerunds.csv'
    plinkingverbs = ROOT + r'\knowledge\linkingverbs.csv'
    pnegators = ROOT + r'\knowledge\negators.csv'
    pnouns = ROOT + r'\knowledge\nouns.csv'
    porientation = ROOT + r'\knowledge\orientation.csv'
    pprepositions = ROOT + r'\knowledge\prepositions.csv'
    psubjects = ROOT + r'\knowledge\subjects.csv'
    pverbs = ROOT + r'\knowledge\verbs.csv'
    pconjunctions = ROOT + r'\knowledge\conjunctions.csv'
    pdtcs = ROOT + r'\knowledge\dtcs.csv'
    pconcerncategories = ROOT + r'\knowledge\concerncategories.csv'
    keytags = ['subject','negative_concern','positive_concern','adjective','adverb','determiner','gerund',\
                   'linkingverb','negator','conjunction','noun','orientation','preposition','verb','dtcs']

    def __init__(self):
        self.key_set_tuple = self._get_keyset_tuple()
        self.concern_categories = self._get_concerncategories()



    def _get_keyset_tuple(self):
        output = []
        # order does potentially matter
        # currently the "first category" field is being used in the wordchain, and subject and concern take priority
        # once they are set, nothing that comes after them will overwrite them

        paths = [Categorizer.psubjects,Categorizer.pnegative_concerns,Categorizer.ppositive_concerns,Categorizer.padjectives, Categorizer.padverbs,\
                 Categorizer.pdeterminers,Categorizer.pgerunds,Categorizer.plinkingverbs,Categorizer.pnegators,Categorizer.pconjunctions,\
                 Categorizer.pnouns,Categorizer.porientation,Categorizer.pprepositions,Categorizer.pverbs,Categorizer.pdtcs]

        keytags = Categorizer.keytags


        sets = []
        try:
            for path in paths:
                sets.append(dataio.csvtoset2(path))
        except Exception:
            print('Error in Categorizer.get_keyset_tuple()')

        for x in range(0,len(keytags)):
            output.append((keytags[x],sets[x]))

        return output

    def set_categories(self, wordnode):
        categories = set()
        comprehended = False
        for item in self.key_set_tuple:
            myset = item[1]
            if wordnode.word in myset:
                categories.add(item[0])
                if wordnode.first_category == '':
                    wordnode.first_category = item[0]
                comprehended = True

        if not comprehended:
            wordnode.first_category = Categorizer.UNKNOWN
            categories.add(Categorizer.UNKNOWN)
        wordnode.categories = categories

    def _get_concerncategories(self):

        table = dataio.Table(Categorizer.pconcerncategories)
        cmap = {} #category map; maps a category to a column number
        dictionary = {}
        i = 1

        for val in table.headers:
            cmap[i] = val
            i = i+1
        #print(cmap)

        for value in cmap.values():
            dictionary[value] = set()

        #print(dictionary)

        i = 1

        for row in table.data:
            for val in row:
                if val != '':
                    category = cmap[i] #find the appropriate category given the column nuymber
                    dictionary[category].add(val) #add the current value to the categories word set
                    i +=1
            i = 1 #reset after each row
        #print(dictionary)

        return dictionary


class ChainBuilder:

    def __init__(self):
        self.__re_wordsonly = re.compile(r'[^ \w]')
        self.categorizer = Categorizer()


    def getwordchain(self, string):
        string = self.__process_str(string)
        words = string.split()
        output = WordChain()

        for word in words:
            node = output.add(word)
            self.categorizer.set_categories(node)

        return output


    def __filter_nonwords(self, matches):
        if matches:
            return ''

    def __process_str(self, str):
        str = str.lower()
        str = self.__re_wordsonly.sub(self.__filter_nonwords, str)
        return str