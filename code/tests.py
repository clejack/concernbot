import dataio as dio
import wordchain as wc
import chainparser2 as cp2
import os
import pdb
import bots

def test():
    initialpath = os.getcwd()
    rootpath = os.path.dirname(initialpath)
    path = rootpath + r'\knowledge'

    print(initialpath)
    print(rootpath)
    print(path)

    print('csvtoset testing')
    print('path error check')

    dio.csvtoset('garbage', 1)

    print('column bounds error check')

    dio.csvtoset(path + r'\linkingverbs.csv', 1000000)

    print('legitimate parameter test')

    myset = dio.csvtoset(path + r'\linkingverbs.csv', 1)
    print(len(myset))
    print(myset)

    table = dio.Table(r'C:\Users\Cjaqs\Desktop\_CustomerConcernBot\Data\2testclaims.csv')
    print(table.headers)
    print(table.data)

def wordchaintests():
    string = "the shock. is le-aking"
    altstring = "REPLACE LENS AND BEZEL OF INSTRUTMENT CLUSTER\
C/S CLUSTER IS COMING APART\
INSPECT AND FIND LENS AND BEZEL COMING APART WITH POOR FIT"
    #pdb.set_trace()
    cb = wc.ChainBuilder()
    chain = cb.getwordchain(altstring)


    tup = chain.str_tuple()
    print(tup[0])
    print(tup[1])


def parsertest():
    string = 'the shock is reall really leaking'
    string2 = "the shock. is le-aking from the right+ side below the rear and the window is broken"
    string2a = 'window is broken'
    string3 = "the shock. is le-aking from the right+ side and there is a leak from the window"
    string6 = 'the vehicle isnt working'
    string7 = 'leaking shock'
    string8 = 'vibration noise from above lf window'
    string9 = 'this is a test for seek with leaks im making a failure to see leaking shocks'
    string10 = 'cmp customer states transmission is jerking'
    bigstring = "REPLACE LENS AND BEZEL OF INSTRUTMENT CLUSTER C/S CLUSTER IS COMING APART INSPECT AND FIND LENS AND BEZEL COMING APART WITH POOR FIT"

    string4 = "leak from the window"
    pattern = ['subject','+','linkingverb','>>','negative_concern','+']
    pattern2 = ['subject','+','linkingverb','>>','positive_concern','<','negator']
    pattern3 = ['negative_concern','+','!<','linkingverb','>','preposition', '>', 'subject','+']
    pattern5 = ['subject','+', 'negative_concern','+']
    pattern6 = ['subject','+', '>','negator', 'positive_concern']
    pattern7 = ['negative_concern','!<','linkingverb', 'subject','+']
    pattern8 = ['negative_concern','+', 'preposition', 'positive_concern']
    pattern9 = ['negative_concern','+', 'subject','+']

    cb = wc.ChainBuilder()
    #parser = cp.ChainParser()
    parser = cp2.ChainParser()
    parser.setpattern(pattern)

    chain = cb.getwordchain(bigstring)
    print(chain)

    parser.search(chain) ##currently ending with a match found for window with pattern2 string2 combo -fix it

    matches = parser.getmatches()
    print("matches found", len(matches.worditems))
    for item in matches.worditems:
        print(item)



    #currently need to implement conjunctions
    #need to implement stopwords in the lookahead state most likely with set intersection followed by a length check
    #implement preceding logic

def columntest():
    table = dio.Table(r'C:\Users\Cjaqs\Desktop\_CustomerConcernBot\Data\2testclaims.csv')

    text = table.getTextfromColumns([10])

    print(text)

def bottest():
    bot = bots.Bot(r'C:\Users\Cjaqs\Desktop\3000testclaims.csv')
    path = r'output.csv'
    bot.prepend_data([10])
    bot.export(path)

def stemtest():
    test = bots.basicstem('leaks')
    print(test)

    test = bots.basicstem('leaked')
    print(test)

    test = bots.basicstem('leaking')
    print(test)

def main():
    #parsertest()
    bottest()
    #wordchaintests()




if __name__ == "__main__":
    main()

