"""
Copyright (C) 2017 Charles Jackson - All Rights Reserved
Created on Sat Sep 9 10:51:00 2017

@author: Charles Jackson
"""
import csv
import os
import re


def errio(error, path):
    print("There was an error with the file at " + path)


def errindex(error, num):
    string = str(num)
    print('the value ' + string + ' was too big/did not exist/did not contain data')


def csvtoset(path, column):
    output = set()
    column = column - 1 # shift to 0 index
    try:
        with open(path, 'rb') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                val = row[column]
                if val != '':
                    output.add(val)
    except IOError as e:
        errio(e, path)
    except IndexError as e:
        errindex(e, column)
    return output

def csvtoset2(path):
    output = set()
    try:
        with open(path, 'rb') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                for val in row:
                    if val != '':
                        output.add(val)
    except IOError as e:
        errio(e, path)
    return output





class Table:
    def __init__(self, path):

        self.headers = []
        self.data = []

        try:
            with open(path, 'rb') as csvfile:
                reader = csv.reader(csvfile)
                i = 1
                for row in reader:
                    if i == 1:
                        self.headers = row
                    else:
                        self.data.append(row)
                    i = i + 1
        except Exception as e:
            errio(e, path)

    def getTextfromColumns(self,columns = []):
        nonwordfilter = re.compile(r'[^ \w]')


        output = []
        for row in self.data:
            string = ''
            for column in columns:
                string += row[column-1] + ' and ' #-1 for 0 index
            string = nonwordfilter.sub(self.__replace_nonwords, string)
            string = string.lower()
            output.append(string)

        return output

    def __replace_nonwords(self, matches):
        if matches:
            return ''
