"""
Copyright (C) 2017 Charles Jackson - All Rights Reserved
Created on Sat Sep 9 18:16:00 2017

@author: Charles Jackson
"""

"""
Rules

-The parser moves forward to the initial term in a pattern

-Once the initial term is found it is marked as the start

-The parser then moves on to the next term in the pattern

-It immediately stops unless prevented from doing so by a special command

-Patterns should be a list of strings

examples

['subject', 'concern']
    -this looks for a subject immediately followed by a concern and returns the matching words 
    -Returns a Matches object
    -Matches will have a 0 length if no instance of a subject followed by a concern is found
    -Matches returns True if it has at least one match contained otherwise return false
['subject', '>', 'concern'] 
    -this moves to the first subject then keeps moving forward until a concern is found
    -fyi this would be a bad search and return a lot of garbage for finding a problem statement in a string



normal inputs~~~~~~~~~~~~~~~~~~~~~~~~
category terms: 'negative_concern', 'noun', 'subject', 'verb', etc.

special characters~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
move forward with stopwords: '>' go forwards any number of times until the next term in the pattern is found
    #return a failure if a stop word is found or if the search term isn't found within the match limit (i.e. loop limit)

move forward without stopwords: '>>' go forwards any number of times until the next term in the pattern is found

preceded by: '<' go backwards one word after a match to make sure it is preceded by the next search term

not preceded by: '!<' go backwards one word after a match to make sure it is not preceded by the next search term

greedysearch: '+' find an instance of the previous term 1 or more times

word term: 'w' look for any word/category

find anything but next term: ^


find x instances: x = digit from 0 to 9. Search for x consecutive occurences of a term.

optional search: 'opt' tagged to the end of a search, everything after this search is optional, and if a match is found
    on terms preceding the opt declaration, the search will generate a non-zero length match

The search has finished: 'end' the end of the pattern has been reached

variables~~~~~~~~~~~~~~~~~~~~~~~~~~~
stopwords: when a term in this list is found, a failure is set unless the current search term matches the stop word
    e.g. if subject and concern are in the stopwords list and the pattern is 
    ['concern', '>', 'preposition', '>', 'component']
    If the concern is found and a subject is found before the preposition, this search will fail
    Rationale:
    Failure string: My shocks are leaking[concern], and my window[subject] on[preposition] the driver side rattles
    We don't want to pick up "leaking and my window" so the stopword of 'subject' will break this search
    Pass string:  I have rattle[concern] noises from[preposition] driver side window[subject] everytime I drive

findall: True = find multiple unique instances of the same pattern
    False = return immediately after the first match is found.
"""

import wordchain as wc
import copy


# general operation
# 1. instantiate Chain parser
# 2. if a pattern was not passed as a parameter, setpattern()
# 3. search() on a wordchain

# This needs to be broken up into more states with less internal functions in the chain parser
# it has become difficult to follow in it's current stage
# implement an internal monitor that will automatically update state
class ChainParser:
    _ENDFLAG = 'end'
    def __init__(self,stopwords=set(['positive_concern', 'negative_concern', 'conjunction']),\
                 findall=True, matchlimit = 9):

        self.stopwords = stopwords  # set of words  that generate a failed search on the cautious forward state
        self.findall = findall
        self._pattern_stored = []  # for copying only
        self._pattern = []  # necessary to account for list mutability; original stored pattern is copied to this
        self._chain = None  # the main chain
        self._subchain = wc.WordChain()  # current sub chain; this is reset in _failed() and _succeeded()
        # self._start_node = None #the initial node to check it moves forward on a successful match to prevent duplicate searching
        self._active_node = None  # the node that is actively checked--this always start with the state_node
        self._last_search = '' #necessary for greedy search logic
        self._active_search = ''
        self._matches = Matches()
        self._nonwords = set(['>', '>>', '<', '!<', '', '+', 'opt'])
        self._words = set(wc.Categorizer.keytags)
        self._update_count = 0
        self._matchlimit = matchlimit

        self._seek = Seek(self) #loop until the first match is found
        self._compare = Compare(self) #do a comparison on a single word
        self._cautiousforward = CautiousForward(self) #move forward but fail on a match for self.stopwords
        self._forward = Forward(self) #
        self._preceding_match = PrecedingMatch(self)
        self._preceding_nomatch = PrecedingNoMatch(self)
        self._greedy = Greedy(self)
        self._number = Number(self)
        self._optional = Optional(self)
        self._failedmatch = FailedMatch(self)
        self._successfulmatch = SuccessfulMatch(self)
        self._finished = Finished(self)
        self._setnext = SetNext(self)
        self._patternended = PatternEnded(self)
        self._current_state = self._seek


    def getmatches(self):
        return self._matches

    def setpattern(self, apattern):
        self._pattern_stored = copy.copy(apattern)
        self._pattern = copy.copy(apattern)
        self._pattern_stored.append(ChainParser._ENDFLAG)
        self._pattern.append(ChainParser._ENDFLAG)

    def search(self, chain):
        self._chain = chain
        self._active_node = chain.head
        self._pattern = copy.copy(self._pattern_stored)
        self._current_state = self._seek
        #self._matches = Matches()

        if len(self._pattern) < 1:
            raise IndexError('Cannot search on a 0 length pattern, please set a pattern')
        else:
            self._nextsearchterm()

        if self._active_search not in self._words:
            raise Exception("The first search term must be a category. i.e. subject, noun, etc")

        while (self._current_state != self._finished):
            self._current_state.execute()



    def _has_more_chain(self):
        if self._active_node is None:
            return False
        else:
            return True

    def _matchfound(self):
        if self._active_node is not None:
            if self._active_search in self._active_node.categories:
                return True
        else:
            return False

    def _unknown_found(self):
        if wc.Categorizer.UNKNOWN in self._active_node.categories:
            return True
        else:
            return False

    def _nextsearchterm(self):
        if len(self._pattern) > 0:
            self._last_search = self._active_search
            self._active_search = self._pattern.pop(0)

    def _nextword(self):
        if self._active_node is not None:
            self._active_node = self._active_node.nextword

    def _stopword_found(self):
        testset = self._active_node.categories.intersection(self.stopwords)
        if len(testset) > 0:
            return True
        else:
            return False

    def _update_subchain(self):
        self._subchain.addnode(self._active_node)
        if self._active_node is not None:
            self._active_node = self._active_node.nextword
            self._update_count += 1


class Matches:
    def __init__(self):
        self.chainitems = set()
        self.worditems = set()

    def add(self, subchain):
        self.chainitems.add(subchain)
        self.worditems.add(str(subchain))

    def __nonzero__(self):
        output = False
        if len(self.worditems) > 0: output = True
        return output


class State(object):
    def __init__(self, parser):
        self.parser = parser  # points to the parser to help keep t
        pass

    def execute(self):
        pass


class Seek(State):  # loop to the first match; if none is found, the program ends here
    def __init__(self, parser):
        super(Seek, self).__init__(parser)

    def execute(self):
        p = self.parser
        matchfound = False

        while p._has_more_chain():
            #print(p._active_node.word)
            if p._matchfound():
                matchfound = True
                break
            else:
                p._nextword()
        if matchfound:
            #print('successful match found on ', p._active_node.word)
            p._current_state = p._successfulmatch
        else:
            #print('***no initial match found in seek')
            p._current_state = p._finished


class Compare(State): #do a comparison on a single word
    def __init__(self, parser):
        super(Compare, self).__init__(parser)

    def execute(self):
        p = self.parser

        if p._matchfound():
            #print('successful match on ',p._active_node.word)
            p._current_state = p._successfulmatch
        else:
            #print('***comparison failed here at the following word')
            p._current_state = p._failedmatch


class CautiousForward(State):
    def __init__(self, parser):
        super(CautiousForward, self).__init__(parser)

    def execute(self):
        p = self.parser
        initialmatch = p._active_node
        while p._has_more_chain() and p._update_count < p._matchlimit:
            # add the current node to the subchain unless it matches the following search

            if not p._matchfound() and not p._stopword_found():
                p._update_subchain()
            else:
                break

        if p._update_count >= p._matchlimit:
            p.active_node = initialmatch #reset the node to where this state started to avoid throwing away data
        p._current_state = p._setnext


class Forward(State):

    def __init__(self, parser):
        super(Forward, self).__init__(parser)

    def execute(self):
        p = self.parser
        initialmatch = p._active_node
        while p._has_more_chain() and p._update_count < p._matchlimit:
            # add the current node to the subchain unless it matches the following search
            if not p._matchfound():
                p._update_subchain()
            else:
                break

        if p._update_count >= p._matchlimit:
            p.active_node = initialmatch #reset the note to where this state started to avoid throwing away data
        p._current_state = p._setnext


class PrecedingMatch(State):
    def __init__(self, parser):
        super(PrecedingMatch, self).__init__(parser)

    def execute(self):
        p = self.parser
        node = p._active_node.prevword
        if node.prevword is not None:
            node = node.prevword
            if p._active_search in node.categories:
                p._nextsearchterm()
                p._current_state = p._setnext
            else:
                p._current_state = p._failedmatch
        else:
            p._current_state = p._failedmatch


class PrecedingNoMatch(State):
    def __init__(self, parser):
        super(PrecedingNoMatch, self).__init__(parser)

    def execute(self):
        p = self.parser
        node = p._active_node.prevword
        if node.prevword is not None:
            node = node.prevword
            if p._active_search not in node.categories:
                p._nextsearchterm()
                p._current_state = p._setnext
            else:
                p._current_state = p._failedmatch
        else:
            p._current_state = p._failedmatch


class Greedy(State): #search for multiple instances of a search term
    def __init__(self, parser):
        super(Greedy, self).__init__(parser)

    def execute(self):
        p = self.parser

        while p._has_more_chain():
            if p._last_search in p._active_node.categories:
                #print('successful match on ',p._active_node.word)
                p._update_subchain()
            else:
                break

        p._nextsearchterm() #flush out the '+' search term
        # active node is already on the proper node
        #set the next state based on the current active search term
        p._current_state = p._setnext


class Number(State):
    pass


class Optional(State):
    pass

class FailedMatch(State):
    def __init__(self, parser):
        super(FailedMatch, self).__init__(parser)

    def execute(self):
        p = self.parser
        p._pattern = copy.copy(p._pattern_stored) #reset the search pattern
        p._nextsearchterm() #set the initial word
        p._subchain = wc.WordChain() #clear the subchain
        p._current_state = p._seek #search for the initial match again.
        p._update_count = 0


class SuccessfulMatch(State): # if a word match is found do the appropriate updates
    def __init__(self, parser):
        super(SuccessfulMatch, self).__init__(parser)

    def execute(self):
        self.parser._update_subchain()
        self.parser._nextsearchterm()
        self.parser._current_state = self.parser._setnext
        self._update_count = 0

class PatternEnded(State): #if the pattern has ended then a full length match has been found--update matches
    def __init__(self, parser):
        super(PatternEnded, self).__init__(parser)

    def execute(self):
        p = self.parser
        p._matches.add(p._subchain)
        p._subchain = wc.WordChain()  # clear the subchain
        p._pattern = copy.copy(p._pattern_stored)
        p._update_count = 0
        if p.findall: # find more than one match
            p._nextsearchterm()
            p._current_state = p._seek
        else:
            p._current_state = p._finished

class Finished(State): #break out of initial search loop

    def __init__(self, parser):
        super(Finished, self).__init__(parser)

    def execute(self):
        self.parser._update_count = 0


class SetNext(State): # state handler

    def __init__(self, parser):
        super(SetNext, self).__init__(parser)

    def execute(self):
        p = self.parser

        if p._update_count >= p._matchlimit: #len(p._pattern) == 0:
            p._update_count = 0
            p._current_state = p._failedmatch
        elif p._active_search == ChainParser._ENDFLAG: #len(p._pattern) == 0:
            p._current_state = p._patternended
        elif p._active_node is None:
            p._current_state = p._finished
        elif p._active_search in p._words:
            p._current_state = p._compare
        elif p._active_search == '>>':
            p._nextsearchterm() #flush out special character '>>' to get to the actual search
            p._current_state = p._forward
        elif p._active_search == '>':
            p._nextsearchterm() #flush out special character '>>' to get to the actual search
            p._current_state = p._cautiousforward
        elif p._active_search == '<':
            p._nextsearchterm() #flush out special character '>>' to get to the actual search
            p._current_state = p._preceding_match
        elif p._active_search == '!<':
            p._nextsearchterm() #flush out special character '>>' to get to the actual search
            p._current_state = p._preceding_nomatch
        elif p._active_search == '+':
            p._current_state = p._greedy
        else:
            raise ValueError('the search term', p._active_search, ' was not understood')
